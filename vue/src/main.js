import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/css/global.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import * as ElIconModules from '@element-plus/icons'
import Blob from './excel/Blob.js'
import Export2Excel from './excel/Export2Excel.js'
import * as echarts from "echarts"

const app = createApp(App)
for(let iconName in ElIconModules){
    app.component(iconName,ElIconModules[iconName])
}
app.config.globalProperties.$echarts = echarts
app.use(router)
app.use(ElementPlus,{ locale: zhCn,size:'small'})
app.use(Blob)
app.mount('#app')
app.use(Export2Excel)
