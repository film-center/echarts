import { createRouter, createWebHistory } from 'vue-router'
import Layout from "../Layout/Layout";
import Login from "../views/Login";
import Register from "../views/Register";

// 导入各个表格的路径
import licensePlate from '../components/Charts/licensePlate_From'
import productSale from '../components/Charts/productSale_area'
import driverTrip from '../components/Charts/driverTrip_Number'
import sale from '../components/Charts/productSale_count'
import weightPurchase from '../components/Charts/weightPurchase_count'
import averageWaiting from '../components/Charts/averageWaiting_time'
import hourlyFactory from '../components/Charts/hourlyFactory_vehicle'


const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: "/carData",
    children: [
      {path: 'carData',name: 'CarData',component: ()=>import("@/views/CarData")},
      {path: 'person',name: 'Person',component: ()=>import("@/views/Person")},
      {path: 'user',name: 'User',component: ()=>import("@/views/User")},
      {path: 'licensePlate',name: 'licensePlate',component: licensePlate},
      {path: 'productSale',name: 'productSale',component: productSale},
      {path: 'driverTrip',name: 'driverTrip',component: driverTrip},
      {path: 'sale',name: 'sale',component: sale},
      {path: 'weightPurchase',name: 'weightPurchase',component: weightPurchase},
      {path: 'averageWaiting',name: 'averageWaiting',component: averageWaiting},
      {path: 'hourlyFactory',name: 'hourlyFactory',component: hourlyFactory},
      {path: 'charts',name: 'Charts',component: ()=>import("@/views/Charts")},
    ]
  },
  {
    path: '/login', name: 'Login', component: Login
  },
  {
    path: '/register', name: 'Register', component: Register
  }
  ]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
