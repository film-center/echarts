/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : echarts

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 27/12/2021 09:50:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for city_name
-- ----------------------------
DROP TABLE IF EXISTS `city_name`;
CREATE TABLE `city_name`  (
  `plate_two` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of city_name
-- ----------------------------
INSERT INTO `city_name` VALUES ('冀A', '石家庄市');
INSERT INTO `city_name` VALUES ('豫A', '郑州市');
INSERT INTO `city_name` VALUES ('云A', '昆明市');
INSERT INTO `city_name` VALUES ('冀B', '唐山市');
INSERT INTO `city_name` VALUES ('豫B', '开封市');
INSERT INTO `city_name` VALUES ('云B', '东川市');
INSERT INTO `city_name` VALUES ('冀C', '秦皇岛市');
INSERT INTO `city_name` VALUES ('豫C', '洛阳市');
INSERT INTO `city_name` VALUES ('云C', '昭通行署');
INSERT INTO `city_name` VALUES ('冀D', '邯郸市');
INSERT INTO `city_name` VALUES ('豫D', '平顶山市');
INSERT INTO `city_name` VALUES ('云D', '曲靖行署');
INSERT INTO `city_name` VALUES ('冀E', '刑台市');
INSERT INTO `city_name` VALUES ('豫E', '安阳市');
INSERT INTO `city_name` VALUES ('云E', '楚雄彝族');
INSERT INTO `city_name` VALUES ('冀F', '保定市');
INSERT INTO `city_name` VALUES ('豫F', '鹤壁市');
INSERT INTO `city_name` VALUES ('云F', '玉溪行署');
INSERT INTO `city_name` VALUES ('冀G', '张家口市');
INSERT INTO `city_name` VALUES ('豫G', '新乡市');
INSERT INTO `city_name` VALUES ('云G', '红河哈尼族');
INSERT INTO `city_name` VALUES ('冀H', '承德市');
INSERT INTO `city_name` VALUES ('豫H', '焦作市');
INSERT INTO `city_name` VALUES ('云H', '文山壮族苗');
INSERT INTO `city_name` VALUES ('冀J', '沧州市');
INSERT INTO `city_name` VALUES ('豫J', '濮阳市');
INSERT INTO `city_name` VALUES ('云J', '思茅行署');
INSERT INTO `city_name` VALUES ('冀K', '邯郸行署');
INSERT INTO `city_name` VALUES ('豫K', '许昌市');
INSERT INTO `city_name` VALUES ('云K', '西双版纳');
INSERT INTO `city_name` VALUES ('冀L', '刑台行署');
INSERT INTO `city_name` VALUES ('豫L', '漯河市');
INSERT INTO `city_name` VALUES ('云L', '大理白族');
INSERT INTO `city_name` VALUES ('冀N', '保定行署');
INSERT INTO `city_name` VALUES ('豫M', '三门峡市');
INSERT INTO `city_name` VALUES ('云M', '保山行署');
INSERT INTO `city_name` VALUES ('冀P', '张家口行署');
INSERT INTO `city_name` VALUES ('豫N', '商丘行署');
INSERT INTO `city_name` VALUES ('云N', '德宏傣族');
INSERT INTO `city_name` VALUES ('冀Q', '承德行署');
INSERT INTO `city_name` VALUES ('豫P', '周口行署');
INSERT INTO `city_name` VALUES ('云P', '丽江行署');
INSERT INTO `city_name` VALUES ('冀R', '廊坊市');
INSERT INTO `city_name` VALUES ('豫Q', '驻马店行署');
INSERT INTO `city_name` VALUES ('云Q', '怒江傈族');
INSERT INTO `city_name` VALUES ('冀S', '沧州行署');
INSERT INTO `city_name` VALUES ('豫R', '南阳行署');
INSERT INTO `city_name` VALUES ('云R', '迪庆藏族');
INSERT INTO `city_name` VALUES ('冀T', '衡水行署');
INSERT INTO `city_name` VALUES ('豫S', '信阳行署');
INSERT INTO `city_name` VALUES ('云S', '临沧行署');
INSERT INTO `city_name` VALUES ('辽A', '沈阳市');
INSERT INTO `city_name` VALUES ('黑A', '哈尔滨市');
INSERT INTO `city_name` VALUES ('湘A', '');
INSERT INTO `city_name` VALUES ('辽B', '大连市');
INSERT INTO `city_name` VALUES ('黑B', '齐齐哈尔市');
INSERT INTO `city_name` VALUES ('湘B', '株州市');
INSERT INTO `city_name` VALUES ('辽C', '鞍山市');
INSERT INTO `city_name` VALUES ('黑C', '牡丹江市');
INSERT INTO `city_name` VALUES ('湘C', '湘潭市');
INSERT INTO `city_name` VALUES ('辽D', '抚顺市');
INSERT INTO `city_name` VALUES ('黑D', '佳木斯市');
INSERT INTO `city_name` VALUES ('湘D', '衡阳市');
INSERT INTO `city_name` VALUES ('辽E', '本溪市');
INSERT INTO `city_name` VALUES ('黑E', '大庆市');
INSERT INTO `city_name` VALUES ('湘E', '邵阳市');
INSERT INTO `city_name` VALUES ('辽F', '丹东市');
INSERT INTO `city_name` VALUES ('黑F', '伊春市');
INSERT INTO `city_name` VALUES ('湘F', '岳阳市');
INSERT INTO `city_name` VALUES ('辽G', '锦州市');
INSERT INTO `city_name` VALUES ('黑G', '鸡西市');
INSERT INTO `city_name` VALUES ('湘G', '大庸市');
INSERT INTO `city_name` VALUES ('辽H', '营口市');
INSERT INTO `city_name` VALUES ('黑H', '鹤岗市');
INSERT INTO `city_name` VALUES ('湘H', '益阳行署');
INSERT INTO `city_name` VALUES ('辽J', '阜新市');
INSERT INTO `city_name` VALUES ('黑J', '双鸭山市');
INSERT INTO `city_name` VALUES ('湘J', '常德市');
INSERT INTO `city_name` VALUES ('辽K', '辽阳市');
INSERT INTO `city_name` VALUES ('黑K', '七台河市');
INSERT INTO `city_name` VALUES ('湘K', '娄底行署');
INSERT INTO `city_name` VALUES ('辽L', '盘锦市');
INSERT INTO `city_name` VALUES ('黑L', '松花江行署');
INSERT INTO `city_name` VALUES ('湘L', '郴州行署');
INSERT INTO `city_name` VALUES ('辽M', '铁岭市');
INSERT INTO `city_name` VALUES ('黑M', '绥化行署');
INSERT INTO `city_name` VALUES ('湘M', '零陵行署');
INSERT INTO `city_name` VALUES ('辽N', '朝阳市');
INSERT INTO `city_name` VALUES ('黑N', '黑河地区行');
INSERT INTO `city_name` VALUES ('湘N', '杯化行署');
INSERT INTO `city_name` VALUES ('辽P', '葫芦岛市');
INSERT INTO `city_name` VALUES ('黑P', '大兴安岭行');
INSERT INTO `city_name` VALUES ('湘P', '湘西自治州');
INSERT INTO `city_name` VALUES ('皖A', '合肥市');
INSERT INTO `city_name` VALUES ('鲁A', '');
INSERT INTO `city_name` VALUES ('新A', '乌鲁木齐市');
INSERT INTO `city_name` VALUES ('皖B', '芜湖市');
INSERT INTO `city_name` VALUES ('鲁B', '青岛市');
INSERT INTO `city_name` VALUES ('新B', '昌吉回族');
INSERT INTO `city_name` VALUES ('皖C', '蚌埠市');
INSERT INTO `city_name` VALUES ('鲁C', '淄博市');
INSERT INTO `city_name` VALUES ('新C', '石河子市');
INSERT INTO `city_name` VALUES ('皖D', '淮南市');
INSERT INTO `city_name` VALUES ('鲁D', '枣庄市');
INSERT INTO `city_name` VALUES ('新D', '奎屯市');
INSERT INTO `city_name` VALUES ('皖E', '马鞍山市');
INSERT INTO `city_name` VALUES ('鲁E', '东营市');
INSERT INTO `city_name` VALUES ('新E', '博尔塔拉');
INSERT INTO `city_name` VALUES ('皖F', '淮北市');
INSERT INTO `city_name` VALUES ('鲁F', '烟台市');
INSERT INTO `city_name` VALUES ('新F', '伊犁哈萨');
INSERT INTO `city_name` VALUES ('皖G', '铜陵市');
INSERT INTO `city_name` VALUES ('鲁G', '潍坊市');
INSERT INTO `city_name` VALUES ('新G', '塔城行署');
INSERT INTO `city_name` VALUES ('皖H', '安庆市');
INSERT INTO `city_name` VALUES ('鲁H', '济宁市');
INSERT INTO `city_name` VALUES ('新H', '阿勒泰行署');
INSERT INTO `city_name` VALUES ('皖J', '黄山市');
INSERT INTO `city_name` VALUES ('鲁J', '泰安市');
INSERT INTO `city_name` VALUES ('新J', '克拉玛依市');
INSERT INTO `city_name` VALUES ('皖K', '阜阳行署');
INSERT INTO `city_name` VALUES ('鲁K', '威海市');
INSERT INTO `city_name` VALUES ('新K', '吐鲁番行署');
INSERT INTO `city_name` VALUES ('皖L', '宿县行署');
INSERT INTO `city_name` VALUES ('鲁L', '日照市');
INSERT INTO `city_name` VALUES ('新L', '哈密行署');
INSERT INTO `city_name` VALUES ('皖M', '滁县行署');
INSERT INTO `city_name` VALUES ('鲁M', '惠州行署');
INSERT INTO `city_name` VALUES ('新M', '巴音郭愣');
INSERT INTO `city_name` VALUES ('皖N', '六安行署');
INSERT INTO `city_name` VALUES ('鲁N', '德州行署');
INSERT INTO `city_name` VALUES ('新N', '阿克苏行署');
INSERT INTO `city_name` VALUES ('皖P', '宜城行署');
INSERT INTO `city_name` VALUES ('鲁P', '聊城行署');
INSERT INTO `city_name` VALUES ('新P', '克孜勒苏柯');
INSERT INTO `city_name` VALUES ('皖Q', '巢湖行署');
INSERT INTO `city_name` VALUES ('鲁Q', '临沂行署');
INSERT INTO `city_name` VALUES ('新Q', '喀什行署');
INSERT INTO `city_name` VALUES ('皖R', '池州行署');
INSERT INTO `city_name` VALUES ('鲁R', '荷泽行署');
INSERT INTO `city_name` VALUES ('新R', '和田行署　');
INSERT INTO `city_name` VALUES ('鲁S', '莱芜市');
INSERT INTO `city_name` VALUES ('苏A', '南京市');
INSERT INTO `city_name` VALUES ('浙A', '杭州市');
INSERT INTO `city_name` VALUES ('赣A', '南昌市');
INSERT INTO `city_name` VALUES ('苏B', '无锡市');
INSERT INTO `city_name` VALUES ('浙B', '宁波市');
INSERT INTO `city_name` VALUES ('赣B', '赣州行署');
INSERT INTO `city_name` VALUES ('苏C', '徐州市');
INSERT INTO `city_name` VALUES ('浙C', '温州市');
INSERT INTO `city_name` VALUES ('赣C', '宜春行署');
INSERT INTO `city_name` VALUES ('苏D', '常州市');
INSERT INTO `city_name` VALUES ('浙D', '绍兴市');
INSERT INTO `city_name` VALUES ('赣D', '吉安行署');
INSERT INTO `city_name` VALUES ('苏E', '苏州市');
INSERT INTO `city_name` VALUES ('浙E', '湖州市');
INSERT INTO `city_name` VALUES ('赣E', '上饶行署');
INSERT INTO `city_name` VALUES ('苏F', '南通市');
INSERT INTO `city_name` VALUES ('浙F', '嘉兴市');
INSERT INTO `city_name` VALUES ('赣F', '抚州行署');
INSERT INTO `city_name` VALUES ('苏G', '连云港市');
INSERT INTO `city_name` VALUES ('浙G', '金华市');
INSERT INTO `city_name` VALUES ('赣G', '九江市');
INSERT INTO `city_name` VALUES ('苏H', '淮阴市');
INSERT INTO `city_name` VALUES ('浙H', '巨州市');
INSERT INTO `city_name` VALUES ('赣H', '景德镇市');
INSERT INTO `city_name` VALUES ('苏J', '盐城市');
INSERT INTO `city_name` VALUES ('浙J', '台州行署');
INSERT INTO `city_name` VALUES ('赣J', '萍乡市');
INSERT INTO `city_name` VALUES ('苏K', '扬州市');
INSERT INTO `city_name` VALUES ('浙K', '丽水行署');
INSERT INTO `city_name` VALUES ('赣K', '新余市');
INSERT INTO `city_name` VALUES ('苏L', '镇江市');
INSERT INTO `city_name` VALUES ('浙L', '舟山市');
INSERT INTO `city_name` VALUES ('赣L', '鹰潭市');
INSERT INTO `city_name` VALUES ('鄂A', '武汉市');
INSERT INTO `city_name` VALUES ('桂A', '南宁市');
INSERT INTO `city_name` VALUES ('甘A', '兰州市');
INSERT INTO `city_name` VALUES ('鄂B', '黄石市');
INSERT INTO `city_name` VALUES ('桂B', '柳州市');
INSERT INTO `city_name` VALUES ('甘B', '嘉峪关市');
INSERT INTO `city_name` VALUES ('鄂C', '十堰市');
INSERT INTO `city_name` VALUES ('桂C', '桂林市');
INSERT INTO `city_name` VALUES ('甘C', '金昌市');
INSERT INTO `city_name` VALUES ('鄂D', '沙市市');
INSERT INTO `city_name` VALUES ('桂D', '梧州市');
INSERT INTO `city_name` VALUES ('甘D', '白银市');
INSERT INTO `city_name` VALUES ('鄂E', '宜昌市');
INSERT INTO `city_name` VALUES ('桂E', '北海市');
INSERT INTO `city_name` VALUES ('甘E', '天水市');
INSERT INTO `city_name` VALUES ('鄂F', '襄樊市');
INSERT INTO `city_name` VALUES ('桂F', '南宁行署');
INSERT INTO `city_name` VALUES ('甘F', '洒泉行署');
INSERT INTO `city_name` VALUES ('鄂G', '鄂州市');
INSERT INTO `city_name` VALUES ('桂G', '柳州行署');
INSERT INTO `city_name` VALUES ('甘G', '张掖行署');
INSERT INTO `city_name` VALUES ('鄂H', '荆门市');
INSERT INTO `city_name` VALUES ('桂H', '桂林行署');
INSERT INTO `city_name` VALUES ('甘H', '武威行署');
INSERT INTO `city_name` VALUES ('鄂J', '黄岗行署');
INSERT INTO `city_name` VALUES ('桂J', '梧州行署');
INSERT INTO `city_name` VALUES ('甘J', '定西行署');
INSERT INTO `city_name` VALUES ('鄂K', '孝感行署');
INSERT INTO `city_name` VALUES ('桂K', '玉林行署');
INSERT INTO `city_name` VALUES ('甘K', '陇南行署');
INSERT INTO `city_name` VALUES ('鄂L', '咸宁行署');
INSERT INTO `city_name` VALUES ('桂L', '河池行署');
INSERT INTO `city_name` VALUES ('甘L', '平凉行署');
INSERT INTO `city_name` VALUES ('鄂M', '荆州行署');
INSERT INTO `city_name` VALUES ('桂M', '百色行署');
INSERT INTO `city_name` VALUES ('甘M', '庆阳行署');
INSERT INTO `city_name` VALUES ('鄂N', '郧阳行署');
INSERT INTO `city_name` VALUES ('桂N', '钦州行署');
INSERT INTO `city_name` VALUES ('甘N', '临夏回族');
INSERT INTO `city_name` VALUES ('鄂P', '宜昌行署');
INSERT INTO `city_name` VALUES ('桂P', '防城港区');
INSERT INTO `city_name` VALUES ('甘P', '甘南藏族');
INSERT INTO `city_name` VALUES ('鄂Q', '鄂西自治州　');
INSERT INTO `city_name` VALUES ('晋A', '太原市');
INSERT INTO `city_name` VALUES ('蒙A', '呼和浩特市');
INSERT INTO `city_name` VALUES ('陕A', '西安市');
INSERT INTO `city_name` VALUES ('晋B', '大同市');
INSERT INTO `city_name` VALUES ('蒙B', '包头市');
INSERT INTO `city_name` VALUES ('陕B', '铜川市');
INSERT INTO `city_name` VALUES ('晋C', '阳泉市');
INSERT INTO `city_name` VALUES ('蒙C', '乌海市');
INSERT INTO `city_name` VALUES ('陕C', '宝鸡市');
INSERT INTO `city_name` VALUES ('晋D', '长治市');
INSERT INTO `city_name` VALUES ('蒙D', '赤峰市');
INSERT INTO `city_name` VALUES ('陕D', '威阳市');
INSERT INTO `city_name` VALUES ('晋E', '晋城市');
INSERT INTO `city_name` VALUES ('蒙E', '呼和浩特市');
INSERT INTO `city_name` VALUES ('陕E', '渭南行署');
INSERT INTO `city_name` VALUES ('晋F', '朔州市');
INSERT INTO `city_name` VALUES ('蒙F', '兴安盟');
INSERT INTO `city_name` VALUES ('陕F', '汉中行署');
INSERT INTO `city_name` VALUES ('晋G', '雁北行署');
INSERT INTO `city_name` VALUES ('蒙G', '锡林郭勒盟');
INSERT INTO `city_name` VALUES ('陕G', '安康行署');
INSERT INTO `city_name` VALUES ('晋H', '忻州行署');
INSERT INTO `city_name` VALUES ('蒙H', '乌兰察布盟');
INSERT INTO `city_name` VALUES ('陕H', '延安行署');
INSERT INTO `city_name` VALUES ('晋J', '吕梁行署');
INSERT INTO `city_name` VALUES ('蒙J', '伊克昭盟');
INSERT INTO `city_name` VALUES ('陕J', '商洛行署');
INSERT INTO `city_name` VALUES ('晋K', '晋中行署');
INSERT INTO `city_name` VALUES ('蒙K', '巴彦淖尔盟');
INSERT INTO `city_name` VALUES ('陕K', '榆林行署');
INSERT INTO `city_name` VALUES ('晋L', '临汾行署');
INSERT INTO `city_name` VALUES ('蒙L', '阿拉善盟　');
INSERT INTO `city_name` VALUES ('晋M', '运城行署　');
INSERT INTO `city_name` VALUES ('吉A', '长春市');
INSERT INTO `city_name` VALUES ('闽A', '福州市');
INSERT INTO `city_name` VALUES ('贵A', '贵阳市');
INSERT INTO `city_name` VALUES ('吉B', '吉林市');
INSERT INTO `city_name` VALUES ('闽B', '莆田市');
INSERT INTO `city_name` VALUES ('贵B', '六盘水市');
INSERT INTO `city_name` VALUES ('吉C', '四平市');
INSERT INTO `city_name` VALUES ('闽C', '泉州市');
INSERT INTO `city_name` VALUES ('贵C', '遵义行署');
INSERT INTO `city_name` VALUES ('吉D', '辽源市');
INSERT INTO `city_name` VALUES ('闽D', '厦门市');
INSERT INTO `city_name` VALUES ('贵D', '铜仁行署');
INSERT INTO `city_name` VALUES ('吉E', '通化市');
INSERT INTO `city_name` VALUES ('闽E', '漳州市');
INSERT INTO `city_name` VALUES ('贵E', '黔西南州');
INSERT INTO `city_name` VALUES ('吉F', '浑江市');
INSERT INTO `city_name` VALUES ('闽F', '龙岩行署');
INSERT INTO `city_name` VALUES ('贵F', '毕节行署');
INSERT INTO `city_name` VALUES ('吉G', '白城行署');
INSERT INTO `city_name` VALUES ('闽G', '三明市');
INSERT INTO `city_name` VALUES ('贵G', '安顺行署');
INSERT INTO `city_name` VALUES ('吉H', '延边朝鲜族');
INSERT INTO `city_name` VALUES ('闽H', '南平行署');
INSERT INTO `city_name` VALUES ('贵H', '黔东南州');
INSERT INTO `city_name` VALUES ('闽J', '宁德行署');
INSERT INTO `city_name` VALUES ('贵J', '黔南州');
INSERT INTO `city_name` VALUES ('粤A', '广州市');
INSERT INTO `city_name` VALUES ('川A', '成都市');
INSERT INTO `city_name` VALUES ('青A', '西宁市');
INSERT INTO `city_name` VALUES ('粤B', '深圳市');
INSERT INTO `city_name` VALUES ('川C', '自贡市');
INSERT INTO `city_name` VALUES ('青B', '海东行署');
INSERT INTO `city_name` VALUES ('粤C', '珠海市');
INSERT INTO `city_name` VALUES ('川D', '攀枝花市');
INSERT INTO `city_name` VALUES ('青C', '海北州');
INSERT INTO `city_name` VALUES ('粤D', '汕头市');
INSERT INTO `city_name` VALUES ('川E', '泸州市');
INSERT INTO `city_name` VALUES ('青D', '黄南行署');
INSERT INTO `city_name` VALUES ('粤E', '佛山市');
INSERT INTO `city_name` VALUES ('川F', '德阳市');
INSERT INTO `city_name` VALUES ('青E', '海南州');
INSERT INTO `city_name` VALUES ('粤F', '韶关市');
INSERT INTO `city_name` VALUES ('川G', '绵阳市');
INSERT INTO `city_name` VALUES ('青F', '果洛州');
INSERT INTO `city_name` VALUES ('粤G', '湛江市');
INSERT INTO `city_name` VALUES ('川H', '广元市');
INSERT INTO `city_name` VALUES ('青G', '玉树州');
INSERT INTO `city_name` VALUES ('粤H', '肇庆市');
INSERT INTO `city_name` VALUES ('川J', '遂宁市');
INSERT INTO `city_name` VALUES ('青H', '海西州');
INSERT INTO `city_name` VALUES ('宁A', '银川市');
INSERT INTO `city_name` VALUES ('渝A', '重庆市区（江南）');
INSERT INTO `city_name` VALUES ('宁B', '石嘴山市');
INSERT INTO `city_name` VALUES ('渝B', '重庆市区（江北）');
INSERT INTO `city_name` VALUES ('宁C', '银南行署');
INSERT INTO `city_name` VALUES ('渝C', '永川区');
INSERT INTO `city_name` VALUES ('宁D', '固原行署');
INSERT INTO `city_name` VALUES ('渝F', '万州区');
INSERT INTO `city_name` VALUES ('渝G', '涪陵区');
INSERT INTO `city_name` VALUES ('渝H', '黔江区');

-- ----------------------------
-- Table structure for city_str
-- ----------------------------
DROP TABLE IF EXISTS `city_str`;
CREATE TABLE `city_str`  (
  `city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of city_str
-- ----------------------------
INSERT INTO `city_str` VALUES ('平顶山市', 2);
INSERT INTO `city_str` VALUES ('濮阳市', 2);
INSERT INTO `city_str` VALUES ('漯河市', 7);
INSERT INTO `city_str` VALUES ('周口行署', 4);
INSERT INTO `city_str` VALUES ('驻马店行署', 32);
INSERT INTO `city_str` VALUES ('南阳行署', 8);
INSERT INTO `city_str` VALUES ('信阳行署', 29);
INSERT INTO `city_str` VALUES ('淮南市', 2);
INSERT INTO `city_str` VALUES ('阜阳行署', 5);
INSERT INTO `city_str` VALUES ('六安行署', 1);
INSERT INTO `city_str` VALUES ('聊城行署', 1);
INSERT INTO `city_str` VALUES ('襄樊市', 6);

-- ----------------------------
-- Table structure for dw_company_avg_wait
-- ----------------------------
DROP TABLE IF EXISTS `dw_company_avg_wait`;
CREATE TABLE `dw_company_avg_wait`  (
  `company_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `minute_avg` float NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_company_avg_wait
-- ----------------------------
INSERT INTO `dw_company_avg_wait` VALUES ('广西交投（国强）', 427.23);
INSERT INTO `dw_company_avg_wait` VALUES ('信阳宏飞建材有限公司', 412.4);
INSERT INTO `dw_company_avg_wait` VALUES ('信阳市健原实业有限公司（矿粉）', 637.27);
INSERT INTO `dw_company_avg_wait` VALUES ('袁斌斌（驻马店）', 357.16);
INSERT INTO `dw_company_avg_wait` VALUES ('中垠物产（长葛）', 437);
INSERT INTO `dw_company_avg_wait` VALUES ('确山县盘龙建材有限公司', 425.5);
INSERT INTO `dw_company_avg_wait` VALUES ('江苏苏瑞嘉（国强）', 575);
INSERT INTO `dw_company_avg_wait` VALUES ('驻马店市源泽贸易有限公司', 399);
INSERT INTO `dw_company_avg_wait` VALUES ('安徽豫信(亳州）', 123);
INSERT INTO `dw_company_avg_wait` VALUES ('漯河安信（周口）', 751.75);
INSERT INTO `dw_company_avg_wait` VALUES ('许昌安信（长葛）', 234.5);
INSERT INTO `dw_company_avg_wait` VALUES ('河南安信（濮阳）', 35);
INSERT INTO `dw_company_avg_wait` VALUES ('武汉信钢（沙洋）', 273.5);
INSERT INTO `dw_company_avg_wait` VALUES ('确山同力水泥', 744.4);
INSERT INTO `dw_company_avg_wait` VALUES ('南阳安信（二郎平）', 315);
INSERT INTO `dw_company_avg_wait` VALUES ('信阳直销（潢川）', 434);
INSERT INTO `dw_company_avg_wait` VALUES ('潢川县福玲商贸有限公司', 401);
INSERT INTO `dw_company_avg_wait` VALUES ('安徽豫信（安庆）', 275.25);
INSERT INTO `dw_company_avg_wait` VALUES ('吴刚', 575);
INSERT INTO `dw_company_avg_wait` VALUES ('确山县冠龙建材有限公司', 663.8);
INSERT INTO `dw_company_avg_wait` VALUES ('项城市峰兴商贸有限公司', 559);
INSERT INTO `dw_company_avg_wait` VALUES ('信阳市明港北源建材有限公司', 878.42);

-- ----------------------------
-- Table structure for dw_company_buy_weight
-- ----------------------------
DROP TABLE IF EXISTS `dw_company_buy_weight`;
CREATE TABLE `dw_company_buy_weight`  (
  `company_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `weight_cnt` float NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_company_buy_weight
-- ----------------------------
INSERT INTO `dw_company_buy_weight` VALUES ('广西交投（国强）', 1055.14);
INSERT INTO `dw_company_buy_weight` VALUES ('信阳宏飞建材有限公司', 371.29);
INSERT INTO `dw_company_buy_weight` VALUES ('信阳市健原实业有限公司（矿粉）', 860);
INSERT INTO `dw_company_buy_weight` VALUES ('袁斌斌（驻马店）', 201);
INSERT INTO `dw_company_buy_weight` VALUES ('中垠物产（长葛）', 72.64);
INSERT INTO `dw_company_buy_weight` VALUES ('确山县盘龙建材有限公司', 67.75);
INSERT INTO `dw_company_buy_weight` VALUES ('江苏苏瑞嘉（国强）', 36.2);
INSERT INTO `dw_company_buy_weight` VALUES ('驻马店市源泽贸易有限公司', 129.3);
INSERT INTO `dw_company_buy_weight` VALUES ('安徽豫信(亳州）', 47.55);
INSERT INTO `dw_company_buy_weight` VALUES ('漯河安信（周口）', 132.34);
INSERT INTO `dw_company_buy_weight` VALUES ('许昌安信（长葛）', 200.3);
INSERT INTO `dw_company_buy_weight` VALUES ('河南安信（濮阳）', 33.25);
INSERT INTO `dw_company_buy_weight` VALUES ('武汉信钢（沙洋）', 72.5);
INSERT INTO `dw_company_buy_weight` VALUES ('确山同力水泥', 164.84);
INSERT INTO `dw_company_buy_weight` VALUES ('南阳安信（二郎平）', 243.15);
INSERT INTO `dw_company_buy_weight` VALUES ('信阳直销（潢川）', 33.29);
INSERT INTO `dw_company_buy_weight` VALUES ('潢川县福玲商贸有限公司', 67.44);
INSERT INTO `dw_company_buy_weight` VALUES ('安徽豫信（安庆）', 136.25);
INSERT INTO `dw_company_buy_weight` VALUES ('吴刚', 34.75);
INSERT INTO `dw_company_buy_weight` VALUES ('确山县冠龙建材有限公司', 173.14);
INSERT INTO `dw_company_buy_weight` VALUES ('项城市峰兴商贸有限公司', 34.5);
INSERT INTO `dw_company_buy_weight` VALUES ('信阳市明港北源建材有限公司', 422.54);

-- ----------------------------
-- Table structure for dw_hour_leave_plate
-- ----------------------------
DROP TABLE IF EXISTS `dw_hour_leave_plate`;
CREATE TABLE `dw_hour_leave_plate`  (
  `hours` int(0) NULL DEFAULT NULL,
  `plate_cnt` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_hour_leave_plate
-- ----------------------------
INSERT INTO `dw_hour_leave_plate` VALUES (12, 1);
INSERT INTO `dw_hour_leave_plate` VALUES (11, 15);
INSERT INTO `dw_hour_leave_plate` VALUES (10, 18);
INSERT INTO `dw_hour_leave_plate` VALUES (9, 9);
INSERT INTO `dw_hour_leave_plate` VALUES (8, 13);
INSERT INTO `dw_hour_leave_plate` VALUES (7, 11);
INSERT INTO `dw_hour_leave_plate` VALUES (6, 8);
INSERT INTO `dw_hour_leave_plate` VALUES (5, 7);
INSERT INTO `dw_hour_leave_plate` VALUES (4, 9);
INSERT INTO `dw_hour_leave_plate` VALUES (3, 9);

-- ----------------------------
-- Table structure for dw_plate_address
-- ----------------------------
DROP TABLE IF EXISTS `dw_plate_address`;
CREATE TABLE `dw_plate_address`  (
  `plate_name` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `plate_address` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_plate_address
-- ----------------------------
INSERT INTO `dw_plate_address` VALUES ('豫', '河南省', 84);
INSERT INTO `dw_plate_address` VALUES ('皖', '安徽省', 8);
INSERT INTO `dw_plate_address` VALUES ('鲁', '山东省', 1);
INSERT INTO `dw_plate_address` VALUES ('鄂', '湖北省', 7);

-- ----------------------------
-- Table structure for dw_sell_address
-- ----------------------------
DROP TABLE IF EXISTS `dw_sell_address`;
CREATE TABLE `dw_sell_address`  (
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货地址',
  `address_count` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_sell_address
-- ----------------------------
INSERT INTO `dw_sell_address` VALUES ('长沙市', 16);
INSERT INTO `dw_sell_address` VALUES ('信阳市', 6);
INSERT INTO `dw_sell_address` VALUES ('新野县', 3);
INSERT INTO `dw_sell_address` VALUES ('驻马店市新蔡县', 10);
INSERT INTO `dw_sell_address` VALUES ('山东省聊城市冠县', 2);
INSERT INTO `dw_sell_address` VALUES ('确山县', 7);
INSERT INTO `dw_sell_address` VALUES ('江苏省溧阳市上兴镇', 1);
INSERT INTO `dw_sell_address` VALUES ('谯城区', 1);
INSERT INTO `dw_sell_address` VALUES ('河南省平顶山市郏县', 3);
INSERT INTO `dw_sell_address` VALUES ('信阳市平桥区明港镇', 2);
INSERT INTO `dw_sell_address` VALUES ('濮阳市清风', 1);
INSERT INTO `dw_sell_address` VALUES ('沙洋县', 2);
INSERT INTO `dw_sell_address` VALUES ('固始', 4);
INSERT INTO `dw_sell_address` VALUES ('息县', 1);
INSERT INTO `dw_sell_address` VALUES ('潢川县', 2);
INSERT INTO `dw_sell_address` VALUES ('襄阳市襄州区牛首镇', 1);
INSERT INTO `dw_sell_address` VALUES ('桐城', 3);
INSERT INTO `dw_sell_address` VALUES ('光山银龙保温材料厂', 1);
INSERT INTO `dw_sell_address` VALUES ('黄山', 1);
INSERT INTO `dw_sell_address` VALUES ('武汉市黄陂区后湖镇', 2);
INSERT INTO `dw_sell_address` VALUES ('确山县普会寺', 5);
INSERT INTO `dw_sell_address` VALUES ('周口市项城市', 1);
INSERT INTO `dw_sell_address` VALUES ('安徽阜阳市颍东区插花镇', 3);
INSERT INTO `dw_sell_address` VALUES ('襄阳市襄州区张湾街道办朱庄社区', 6);
INSERT INTO `dw_sell_address` VALUES ('南阳市蒲山镇', 2);
INSERT INTO `dw_sell_address` VALUES ('郸城', 1);
INSERT INTO `dw_sell_address` VALUES ('信阳市明港镇', 4);
INSERT INTO `dw_sell_address` VALUES ('利辛', 1);
INSERT INTO `dw_sell_address` VALUES ('信阳市明港北源建材有限公司', 3);
INSERT INTO `dw_sell_address` VALUES ('随州市高新区', 1);
INSERT INTO `dw_sell_address` VALUES ('武汉市黄陂区横店镇', 3);
INSERT INTO `dw_sell_address` VALUES ('武汉市东西湖区国沙一路', 1);

-- ----------------------------
-- Table structure for dw_sell_goods
-- ----------------------------
DROP TABLE IF EXISTS `dw_sell_goods`;
CREATE TABLE `dw_sell_goods`  (
  `goods_name_group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '其他' COMMENT '物资名称',
  `goods_count` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_sell_goods
-- ----------------------------
INSERT INTO `dw_sell_goods` VALUES ('带钢Q355B', 18);
INSERT INTO `dw_sell_goods` VALUES ('螺纹钢E9', 12);
INSERT INTO `dw_sell_goods` VALUES ('矿渣微粉(科创)', 57);
INSERT INTO `dw_sell_goods` VALUES ('其他', 13);

-- ----------------------------
-- Table structure for dw_transport_numbers
-- ----------------------------
DROP TABLE IF EXISTS `dw_transport_numbers`;
CREATE TABLE `dw_transport_numbers`  (
  `plate_num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌号',
  `plate_count` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_transport_numbers
-- ----------------------------
INSERT INTO `dw_transport_numbers` VALUES ('豫LN6297', 2);
INSERT INTO `dw_transport_numbers` VALUES ('豫SA9166', 1);
INSERT INTO `dw_transport_numbers` VALUES ('鄂FTC990', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB6238', 2);
INSERT INTO `dw_transport_numbers` VALUES ('鄂FGZ672', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫Q29707', 2);
INSERT INTO `dw_transport_numbers` VALUES ('豫Q29905', 1);
INSERT INTO `dw_transport_numbers` VALUES ('鲁PQ0370', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD5038', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖NB6026', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QE1698', 2);
INSERT INTO `dw_transport_numbers` VALUES ('豫Q29863', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖K17735', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE6788', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫DV5888', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QE3289', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB6903', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫JB2039', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫Q28198', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB8070', 2);
INSERT INTO `dw_transport_numbers` VALUES ('豫QE2850', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QG2106', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB8058', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB5975', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫PT3525', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫DL2215', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫LN9136', 2);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE0505', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QE8193', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QE4366', 2);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB8700', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫LN6036', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE5299', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QE6536', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE9193', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QC9735', 1);
INSERT INTO `dw_transport_numbers` VALUES ('鄂FNS289', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫Q27560', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫PV8977', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB3838', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB6995', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫PR5516', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE9293', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖KV0502', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE1668', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD5308', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE7810', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫JA5607', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD7261', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE0586', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫LN0100', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫S39855', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QF7612', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫PN5190', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QG2185', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD5172', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫S58560', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QF0953', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫Q29120', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD5072', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖KU1770', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RPX607', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖D34925', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RAU098', 1);
INSERT INTO `dw_transport_numbers` VALUES ('鄂FLV752', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RQ9577', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RAY390', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫LJ7138', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QF6581', 4);
INSERT INTO `dw_transport_numbers` VALUES ('皖KQ6527', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE3826', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB9953', 3);
INSERT INTO `dw_transport_numbers` VALUES ('鄂S195C6', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SB3732', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE3106', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD8820', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE5036', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫SE9768', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖KU7539', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RXY389', 1);
INSERT INTO `dw_transport_numbers` VALUES ('鄂FNM591', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RJ1029', 1);
INSERT INTO `dw_transport_numbers` VALUES ('皖D50227', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RJ3518', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QG5602', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫QD8811', 1);
INSERT INTO `dw_transport_numbers` VALUES ('鄂FG0115', 1);
INSERT INTO `dw_transport_numbers` VALUES ('豫RQB373', 1);

-- ----------------------------
-- Table structure for dw_user
-- ----------------------------
DROP TABLE IF EXISTS `dw_user`;
CREATE TABLE `dw_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `perms` int(0) NULL DEFAULT NULL COMMENT '权限',
  `nick_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '呢称',
  `sex` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性别 1男 2女 3无性',
  `age` int(0) NULL DEFAULT NULL COMMENT '年龄',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dw_user
-- ----------------------------
INSERT INTO `dw_user` VALUES (1, 'admin', '123456', 1, '管理员', '男', 15, '2021-12-24 13:10:02', '2021-12-25 15:05:09');
INSERT INTO `dw_user` VALUES (2, 'aaa', '12345', 0, '王二', '女', 34, '2021-12-24 13:10:10', '2021-12-25 13:03:08');
INSERT INTO `dw_user` VALUES (3, 'bbb', '123456', 0, '张三', '男', 0, '2021-12-24 13:10:15', '2021-12-25 13:02:58');

-- ----------------------------
-- Table structure for ods_origin
-- ----------------------------
DROP TABLE IF EXISTS `ods_origin`;
CREATE TABLE `ods_origin`  (
  `origin_id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_id` varchar(23) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运输单号',
  `plate_num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌号',
  `unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货单位',
  `leave_date` datetime(0) NULL DEFAULT NULL COMMENT '出厂时间',
  `wait_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '等待卸货时长',
  `goods_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物资名称',
  `weight` float NULL DEFAULT NULL COMMENT '物资重量',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货地址',
  `deliver_unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发货单位',
  PRIMARY KEY (`origin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 624 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ods_origin
-- ----------------------------
INSERT INTO `ods_origin` VALUES (1, 'WYSDD-20211123-00000133', '豫LN6297', '广西交投（国强）', '2021-11-23 12:09:38', '9小时35分钟', '带钢Q355B', 81.55, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (2, 'WYSDD-20211123-00000070', '豫SA9166', '信阳宏飞建材有限公司', '2021-11-23 11:54:43', '14小时03分钟', '矿渣微粉(科创)', 56.2, '信阳市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (3, 'WYSDD-20211123-00000069', '鄂FTC990', '信阳市健原实业有限公司（矿粉）', '2021-11-23 11:53:05', '0小时35分钟', '矿渣微粉', 56.75, '新野县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (4, 'WYSDD-20211123-00000068', '豫SB6238', '信阳宏飞建材有限公司', '2021-11-23 11:52:03', '0小时35分钟', '矿渣微粉(科创)', 32.6, '信阳市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (5, 'WYSDD-20211123-00000067', '鄂FGZ672', '信阳市健原实业有限公司（矿粉）', '2021-11-23 11:51:36', '0小时35分钟', '矿渣微粉', 47.2, '新野县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (6, 'WYSDD-20211123-00000132', '豫Q29707', '广西交投（国强）', '2021-11-23 11:45:06', '0小时15分钟', '带钢Q355B', 81.35, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (7, 'WYSDD-20211123-00000066', '豫Q29905', '袁斌斌（驻马店）', '2021-11-23 11:41:55', '0小时34分钟', '矿渣微粉(科创)', 32.4, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (8, 'WYSDD-20211123-00000131', '鲁PQ0370', '中垠物产（长葛）', '2021-11-23 11:37:31', '10小时08分钟', '带钢235B', 36.3, '山东省聊城市冠县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (9, 'WYSDD-20211123-00000065', '豫QD5038', '确山县盘龙建材有限公司', '2021-11-23 11:35:58', '10小时08分钟', '矿渣微粉', 35.5, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (10, 'WYSDD-20211123-00000130', '皖NB6026', '江苏苏瑞嘉（国强）', '2021-11-23 11:34:19', '9小时35分钟', '带钢195', 36.2, '江苏省溧阳市上兴镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (11, 'WYSDD-20211123-00000129', '豫QE1698', '广西交投（国强）', '2021-11-23 11:25:42', '0小时15分钟', '带钢Q355B', 81.2, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (12, 'WYSDD-20211123-00000064', '豫Q29863', '驻马店市源泽贸易有限公司', '2021-11-23 11:18:03', '4小时05分钟', '矿渣微粉', 33.15, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (13, 'WYSDD-20211123-00000128', '皖K17735', '安徽豫信(亳州）', '2021-11-23 11:15:23', '0小时35分钟', '螺纹钢E9', 12.3, '谯城区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (14, 'WYSDD-20211123-00000063', '豫SE6788', '信阳宏飞建材有限公司', '2021-11-23 11:11:07', '4小时05分钟', '矿渣微粉(科创)', 34.1, '信阳市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (15, 'WYSDD-20211123-00000127', '豫DV5888', '漯河安信（周口）', '2021-11-23 11:10:59', '8小时52分钟', '螺纹钢E12', 32.8, '河南省平顶山市郏县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (16, 'WYSDD-20211123-00000126', '豫QE3289', '许昌安信（长葛）', '2021-11-23 11:08:50', '0小时35分钟', '带钢195', 100.25, '信阳市平桥区明港镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (17, 'WYSDD-20211123-00000062', '豫SB6903', '信阳宏飞建材有限公司', '2021-11-23 10:59:40', '0小时15分钟', '矿渣微粉(科创)', 33.05, '信阳市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (18, 'WYSDD-20211123-00000125', '豫JB2039', '河南安信（濮阳）', '2021-11-23 10:58:56', '0小时35分钟', '螺纹钢E10', 33.25, '濮阳市清风', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (19, 'WYSDD-20211123-00000124', '豫Q28198', '武汉信钢（沙洋）', '2021-11-23 10:56:18', '0小时15分钟', '螺纹钢E12', 36.2, '沙洋县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (20, 'WYSDD-20211123-00000061', '豫SB8070', '确山同力水泥', '2021-11-23 10:54:21', '0小时15分钟', '矿渣微粉(科创)', 32.05, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (21, 'WYSDD-20211123-00000123', '豫QE2850', '南阳安信（二郎平）', '2021-11-23 10:54:07', '4小时05分钟', '螺纹钢E9', 100.3, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (22, 'WYSDD-20211123-00000122', '豫QG2106', '南阳安信（二郎平）', '2021-11-23 10:52:28', '7小时14分钟', '螺纹钢E9', 100, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (23, 'WYSDD-20211123-00000060', '豫SB8058', '信阳宏飞建材有限公司', '2021-11-23 10:42:39', '8小时52分钟', '矿渣微粉(科创)', 32.9, '固始', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (24, 'WYSDD-20211123-00000059', '豫SB5975', '信阳宏飞建材有限公司', '2021-11-23 10:40:37', '0小时15分钟', '矿渣微粉(科创)', 32.75, '固始', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (25, 'WYSDD-20211123-00000121', '豫PT3525', '漯河安信（周口）', '2021-11-23 10:39:02', '7小时14分钟', '盘螺E', 33.3, '河南省平顶山市郏县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (26, 'WYSDD-20211123-00000120', '豫DL2215', '漯河安信（周口）', '2021-11-23 10:37:20', '9小时35分钟', '盘螺E', 33.2, '河南省平顶山市郏县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (27, 'WYSDD-20211123-00000119', '豫LN9136', '广西交投（国强）', '2021-11-23 10:35:19', '7小时14分钟', '带钢Q355B', 81, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (28, 'WYSDD-20211123-00000058', '豫SE0505', '袁斌斌（驻马店）', '2021-11-23 10:31:48', '8小时52分钟', '矿渣微粉(科创)', 34.4, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (29, 'WYSDD-20211123-00000118', '豫QE8193', '许昌安信（长葛）', '2021-11-23 10:28:41', '7小时14分钟', '带钢195', 100.05, '信阳市平桥区明港镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (30, 'WYSDD-20211123-00000117', '豫QE4366', '广西交投（国强）', '2021-11-23 10:27:28', '8小时52分钟', '带钢Q355B', 81.1, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (31, 'WYSDD-20211123-00000057', '豫SB8700', '确山同力水泥', '2021-11-23 10:24:04', '8小时52分钟', '矿渣微粉(科创)', 33.6, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (32, 'WYSDD-20211123-00000116', '豫LN6036', '信阳直销（潢川）', '2021-11-23 10:15:49', '7小时14分钟', '盘螺E', 33.3, '息县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (33, 'WYSDD-20211123-00000056', '豫SE5299', '确山同力水泥', '2021-11-23 10:07:41', '4小时03分钟', '矿渣微粉', 33.7, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (34, 'WYSDD-20211123-00000115', '豫QE6536', '武汉信钢（沙洋）', '2021-11-23 10:07:22', '8小时52分钟', '螺纹钢E12', 36.3, '沙洋县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (35, 'WYSDD-20211123-00000055', '豫SE9193', '潢川县福玲商贸有限公司', '2021-11-23 09:56:45', '4小时03分钟', '矿渣微粉(科创)', 33.6, '潢川县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (36, 'WYSDD-20211123-00000054', '豫QC9735', '确山县盘龙建材有限公司', '2021-11-23 09:53:02', '4小时03分钟', '矿渣微粉', 32.25, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (37, 'WYSDD-20211123-00000053', '鄂FNS289', '信阳市健原实业有限公司（矿粉）', '2021-11-23 09:47:32', '1小时35分钟', '矿渣微粉', 33.35, '襄阳市襄州区牛首镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (38, 'WYSDD-20211123-00000114', '豫Q27560', '广西交投（国强）', '2021-11-23 09:46:34', '4小时03分钟', '带钢Q355B', 81.35, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (39, 'WYSDD-20211123-00000113', '豫PV8977', '广西交投（国强）', '2021-11-23 09:43:22', '8小时16分钟', '带钢Q355B', 80.75, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (40, 'WYSDD-20211123-00000112', '豫SB3838', '安徽豫信（安庆）', '2021-11-23 09:38:04', '0小时15分钟', '螺纹钢E12', 32.8, '桐城', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (41, 'WYSDD-20211123-00000111', '豫SB6995', '安徽豫信（安庆）', '2021-11-23 09:33:32', '1小时35分钟', '盘螺', 33.15, '桐城', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (42, 'WYSDD-20211123-00000110', '豫PR5516', '广西交投（国强）', '2021-11-23 09:20:51', '8小时16分钟', '带钢Q355B', 80.85, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (43, 'WYSDD-20211123-00000109', '豫SE9293', '吴刚', '2021-11-23 09:04:51', '9小时35分钟', '废干渣块', 34.75, '光山银龙保温材料厂', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (44, 'WYSDD-20211123-00000108', '皖KV0502', '安徽豫信（安庆）', '2021-11-23 08:48:02', '8小时16分钟', '螺纹钢E12', 35.35, '黄山', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (45, 'WYSDD-20211123-00000107', '豫LN6297', '广西交投（国强）', '2021-11-23 08:45:52', '8小时16分钟', '带钢Q355B', 81.35, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (46, 'WYSDD-20211123-00000052', '豫SE1668', '信阳市健原实业有限公司（矿粉）', '2021-11-23 08:42:52', '4小时26分钟', '矿渣微粉(科创)', 34.05, '武汉市黄陂区后湖镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (47, 'WYSDD-20211123-00000106', '豫QD5308', '南阳安信（二郎平）', '2021-11-23 08:40:30', '4小时26分钟', '盘螺E', 42.85, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (48, 'WYSDD-20211123-00000051', '豫SE7810', '袁斌斌（驻马店）', '2021-11-23 08:30:22', '1小时35分钟', '矿渣微粉(科创)', 32.25, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (49, 'WYSDD-20211123-00000105', '豫SB6238', '信阳宏飞建材有限公司', '2021-11-23 08:29:18', '4小时26分钟', '矿渣微粉(科创)', 32.6, '信阳市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (50, 'WYSDD-20211123-00000104', '豫JA5607', '中垠物产（长葛）', '2021-11-23 08:28:01', '4小时26分钟', '带钢235B', 36.35, '山东省聊城市冠县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (51, 'WYSDD-20211123-00000103', '豫Q29707', '广西交投（国强）', '2021-11-23 08:27:03', '9小时35分钟', '带钢Q355B', 81.35, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (52, 'WYSDD-20211123-00000102', '豫QD7261', '安徽豫信（安庆）', '2021-11-23 08:21:13', '8小时15分钟', '螺纹钢E9', 34.95, '桐城', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (53, 'WYSDD-20211123-00000050', '豫SE0586', '信阳宏飞建材有限公司', '2021-11-23 08:20:33', '8小时16分钟', '矿渣微粉(科创)', 34.25, '固始', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (54, 'WYSDD-20211123-00000049', '豫LN0100', '确山县冠龙建材有限公司', '2021-11-23 08:08:14', '8小时16分钟', '矿渣微粉(科创)', 35.1, '确山县普会寺', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (55, 'WYSDD-20211123-00000048', '豫S39855', '潢川县福玲商贸有限公司', '2021-11-23 08:06:29', '9小时19分钟', '矿渣微粉(科创)', 33.85, '潢川县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (56, 'WYSDD-20211123-00000101', '豫QE1698', '广西交投（国强）', '2021-11-23 08:04:57', '9小时19分钟', '带钢Q355B', 81.2, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (57, 'WYSDD-20211123-00000100', '豫LN9136', '广西交投（国强）', '2021-11-23 07:57:24', '9小时19分钟', '带钢Q355B', 81, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (58, 'WYSDD-20211123-00000047', '豫QF7612', '确山县冠龙建材有限公司', '2021-11-23 07:53:16', '9小时19分钟', '矿渣微粉(科创)', 34.1, '确山县普会寺', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (59, 'WYSDD-20211123-00000099', '豫QE4366', '广西交投（国强）', '2021-11-23 07:48:49', '9小时19分钟', '带钢Q355B', 81.1, '长沙市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (60, 'WYSDD-20211123-00000046', '豫PN5190', '项城市峰兴商贸有限公司', '2021-11-23 07:45:15', '9小时19分钟', '矿渣微粉', 34.5, '周口市项城市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (61, 'WYSDD-20211123-00000045', '豫QG2185', '确山县冠龙建材有限公司', '2021-11-23 07:41:12', '8小时52分钟', '矿渣微粉(科创)', 34.65, '确山县普会寺', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (62, 'WYSDD-20211123-00000044', '豫QD5172', '确山县冠龙建材有限公司', '2021-11-23 07:29:30', '24小时26分钟', '矿渣微粉(科创)', 34.05, '确山县普会寺', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (63, 'WYSDD-20211123-00000043', '豫S58560', '信阳宏飞建材有限公司', '2021-11-23 07:19:52', '24小时26分钟', '矿渣微粉(科创)', 50, '信阳市', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (64, 'WYSDD-20211123-00000042', '豫QF0953', '确山县冠龙建材有限公司', '2021-11-23 07:17:08', '4小时26分钟', '矿渣微粉(科创)', 35.25, '确山县普会寺', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (65, 'WYSDD-20211123-00000041', '豫Q29120', '确山同力水泥', '2021-11-23 07:14:42', '24小时26分钟', '矿渣微粉', 33.45, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (66, 'WYSDD-20211123-00000040', '豫QD5072', '袁斌斌（驻马店）', '2021-11-23 07:09:38', '4小时26分钟', '矿渣微粉(科创)', 34.95, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (67, 'WYSDD-20211123-00000039', '皖KU1770', '信阳市健原实业有限公司（矿粉）', '2021-11-23 07:02:22', '24小时26分钟', '矿渣微粉', 32.9, '安徽阜阳市颍东区插花镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (68, 'WYSDD-20211123-00000038', '豫RPX607', '信阳市健原实业有限公司（矿粉）', '2021-11-23 06:57:31', '4小时26分钟', '矿渣微粉(科创)', 36.15, '襄阳市襄州区张湾街道办朱庄社区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (69, 'WYSDD-20211123-00000037', '皖D34925', '信阳市健原实业有限公司（矿粉）', '2021-11-23 06:43:04', '24小时26分钟', '矿渣微粉', 33.35, '安徽阜阳市颍东区插花镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (70, 'WYSDD-20211123-00000036', '豫RAU098', '信阳市健原实业有限公司（矿粉）', '2021-11-23 06:38:29', '4小时26分钟', '矿渣微粉(科创)', 35.85, '襄阳市襄州区张湾街道办朱庄社区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (71, 'WYSDD-20211123-00000035', '鄂FLV752', '信阳市健原实业有限公司（矿粉）', '2021-11-23 06:37:45', '24小时26分钟', '矿渣微粉', 55.2, '南阳市蒲山镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (72, 'WYSDD-20211123-00000034', '豫RQ9577', '信阳市健原实业有限公司（矿粉）', '2021-11-23 06:36:48', '24小时26分钟', '矿渣微粉(科创)', 35, '襄阳市襄州区张湾街道办朱庄社区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (73, 'WYSDD-20211123-00000033', '豫RAY390', '驻马店市源泽贸易有限公司', '2021-11-23 06:27:58', '8小时52分钟', '矿渣微粉', 31.35, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (74, 'WYSDD-20211123-00000098', '豫LJ7138', '漯河安信（周口）', '2021-11-23 06:07:16', '24小时26分钟', '螺纹钢E9', 33.05, '郸城', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (75, 'WYSDD-20211123-00000097', '豫QF6581', '信阳市明港北源建材有限公司', '2021-11-23 06:02:27', '24小时26分钟', '废钢渣（非磁性钢渣）', 66.6, '信阳市明港镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (76, 'WYSDD-20211123-00000096', '皖KQ6527', '安徽豫信(亳州）', '2021-11-23 05:47:47', '3小时31分钟', '螺纹钢E9', 35.25, '利辛', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (77, 'WYSDD-20211123-00000032', '豫SE3826', '信阳市健原实业有限公司（矿粉）', '2021-11-23 05:41:47', '24小时26分钟', '矿渣微粉(科创)', 34.9, '武汉市黄陂区后湖镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (78, 'WYSDD-20211123-00000095', '豫SB9953', '信阳市明港北源建材有限公司', '2021-11-23 05:40:45', '24小时26分钟', '废钢渣（非磁性钢渣）', 52.05, '信阳市明港北源建材有限公司', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (79, 'WYSDD-20211123-00000031', '豫SB8070', '确山同力水泥', '2021-11-23 05:31:17', '24小时26分钟', '矿渣微粉(科创)', 32.05, '确山县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (80, 'WYSDD-20211123-00000030', '鄂S195C6', '信阳市健原实业有限公司（矿粉）', '2021-11-23 05:18:53', '24小时26分钟', '矿渣微粉(科创)', 34.05, '随州市高新区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (81, 'WYSDD-20211123-00000094', '豫QF6581', '信阳市明港北源建材有限公司', '2021-11-23 05:05:31', '24小时26分钟', '废钢渣（非磁性钢渣）', 66.6, '信阳市明港镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (82, 'WYSDD-20211123-00000029', '豫SB3732', '信阳宏飞建材有限公司', '2021-11-23 05:00:35', '3小时31分钟', '矿渣微粉(科创)', 32.85, '固始', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (83, 'WYSDD-20211123-00000028', '豫SE3106', '信阳市健原实业有限公司（矿粉）', '2021-11-23 04:52:31', '10小时08分钟', '矿渣微粉(科创)', 35, '武汉市黄陂区横店镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (84, 'WYSDD-20211123-00000027', '豫QD8820', '袁斌斌（驻马店）', '2021-11-23 04:37:52', '10小时08分钟', '矿渣微粉(科创)', 32.85, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (85, 'WYSDD-20211123-00000093', '豫SB9953', '信阳市明港北源建材有限公司', '2021-11-23 04:35:15', '4小时03分钟', '废钢渣（非磁性钢渣）', 52.05, '信阳市明港北源建材有限公司', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (86, 'WYSDD-20211123-00000026', '豫SE5036', '信阳市健原实业有限公司（矿粉）', '2021-11-23 04:32:50', '10小时08分钟', '矿渣微粉(科创)', 34.2, '武汉市黄陂区横店镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (87, 'WYSDD-20211123-00000025', '豫SE9768', '信阳市健原实业有限公司（矿粉）', '2021-11-23 04:14:12', '4小时03分钟', '矿渣微粉', 33.15, '武汉市东西湖区国沙一路', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (88, 'WYSDD-20211123-00000092', '豫QF6581', '信阳市明港北源建材有限公司', '2021-11-23 04:13:59', '8小时52分钟', '废钢渣（非磁性钢渣）', 66.6, '信阳市明港镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (89, 'WYSDD-20211123-00000024', '皖KU7539', '信阳市健原实业有限公司（矿粉）', '2021-11-23 04:13:44', '4小时03分钟', '矿渣微粉(科创)', 34.15, '武汉市黄陂区横店镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (90, 'WYSDD-20211123-00000023', '豫RXY389', '驻马店市源泽贸易有限公司', '2021-11-23 04:10:00', '10小时08分钟', '矿渣微粉', 32.3, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (91, 'WYSDD-20211123-00000022', '鄂FNM591', '信阳市健原实业有限公司（矿粉）', '2021-11-23 04:02:22', '4小时03分钟', '矿渣微粉', 46.85, '新野县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (92, 'WYSDD-20211123-00000021', '豫RJ1029', '信阳市健原实业有限公司（矿粉）', '2021-11-23 03:59:02', '9小时35分钟', '矿渣微粉(科创)', 45.6, '襄阳市襄州区张湾街道办朱庄社区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (93, 'WYSDD-20211123-00000020', '皖D50227', '信阳市健原实业有限公司（矿粉）', '2021-11-23 03:52:23', '7小时14分钟', '矿渣微粉', 32.6, '安徽阜阳市颍东区插花镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (94, 'WYSDD-20211123-00000019', '豫RJ3518', '信阳市健原实业有限公司（矿粉）', '2021-11-23 03:50:26', '10小时08分钟', '矿渣微粉', 60.25, '南阳市蒲山镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (95, 'WYSDD-20211123-00000091', '豫SB9953', '信阳市明港北源建材有限公司', '2021-11-23 03:46:09', '8小时8分钟', '废钢渣（非磁性钢渣）', 52.05, '信阳市明港北源建材有限公司', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (96, 'WYSDD-20211123-00000018', '豫QG5602', '袁斌斌（驻马店）', '2021-11-23 03:34:19', '10小时08分钟', '矿渣微粉(科创)', 34.15, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (97, 'WYSDD-20211123-00000017', '豫QD8811', '驻马店市源泽贸易有限公司', '2021-11-23 03:32:03', '3小时31分钟', '矿渣微粉', 32.5, '驻马店市新蔡县', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (98, 'WYSDD-20211123-00000090', '豫QF6581', '信阳市明港北源建材有限公司', '2021-11-23 03:31:00', '8小时08分钟', '废钢渣（非磁性钢渣）', 66.6, '信阳市明港镇', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (99, 'WYSDD-20211123-00000016', '鄂FG0115', '信阳市健原实业有限公司（矿粉）', '2021-11-23 03:19:06', '3小时31分钟', '矿渣微粉(科创)', 34, '襄阳市襄州区张湾街道办朱庄社区', '安钢集团信阳钢铁有限责任公司');
INSERT INTO `ods_origin` VALUES (100, 'WYSDD-20211123-00000015', '豫RQB373', '信阳市健原实业有限公司（矿粉）', '2021-11-23 03:08:01', '8小时08分钟', '矿渣微粉(科创)', 35.45, '襄阳市襄州区张湾街道办朱庄社区', '安钢集团信阳钢铁有限责任公司');

-- ----------------------------
-- Table structure for plate_str
-- ----------------------------
DROP TABLE IF EXISTS `plate_str`;
CREATE TABLE `plate_str`  (
  `first_str` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of plate_str
-- ----------------------------
INSERT INTO `plate_str` VALUES ('豫', 84);
INSERT INTO `plate_str` VALUES ('鄂', 7);
INSERT INTO `plate_str` VALUES ('鲁', 1);
INSERT INTO `plate_str` VALUES ('皖', 8);

-- ----------------------------
-- Table structure for province_name
-- ----------------------------
DROP TABLE IF EXISTS `province_name`;
CREATE TABLE `province_name`  (
  `province_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of province_name
-- ----------------------------
INSERT INTO `province_name` VALUES ('京', '北京市');
INSERT INTO `province_name` VALUES ('津', '天津市');
INSERT INTO `province_name` VALUES ('沪', '上海市');
INSERT INTO `province_name` VALUES ('渝', '重庆市');
INSERT INTO `province_name` VALUES ('冀', '河北省');
INSERT INTO `province_name` VALUES ('豫', '河南省');
INSERT INTO `province_name` VALUES ('云', '云南省');
INSERT INTO `province_name` VALUES ('辽', '辽宁省');
INSERT INTO `province_name` VALUES ('黑', '黑龙江省');
INSERT INTO `province_name` VALUES ('湘', '湖南省');
INSERT INTO `province_name` VALUES ('皖', '安徽省');
INSERT INTO `province_name` VALUES ('鲁', '山东省');
INSERT INTO `province_name` VALUES ('新', '新疆维吾尔');
INSERT INTO `province_name` VALUES ('苏', '江苏省');
INSERT INTO `province_name` VALUES ('浙', '浙江省');
INSERT INTO `province_name` VALUES ('赣', '江西省');
INSERT INTO `province_name` VALUES ('鄂', '湖北省');
INSERT INTO `province_name` VALUES ('桂', '广西壮族');
INSERT INTO `province_name` VALUES ('甘', '甘肃省');
INSERT INTO `province_name` VALUES ('晋', '山西省');
INSERT INTO `province_name` VALUES ('蒙', '内蒙古');
INSERT INTO `province_name` VALUES ('陕', '陕西省');
INSERT INTO `province_name` VALUES ('吉', '吉林省');
INSERT INTO `province_name` VALUES ('闽', '福建省');
INSERT INTO `province_name` VALUES ('贵', '贵州省');
INSERT INTO `province_name` VALUES ('粤', '广东省');
INSERT INTO `province_name` VALUES ('青', '青海省');
INSERT INTO `province_name` VALUES ('藏', '西藏');
INSERT INTO `province_name` VALUES ('川', '四川省');
INSERT INTO `province_name` VALUES ('宁', '宁夏回族');
INSERT INTO `province_name` VALUES ('琼', '海南省');

SET FOREIGN_KEY_CHECKS = 1;
