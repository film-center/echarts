package com.example.service;

import com.example.pojo.*;

import java.util.List;

public interface SelectAllDataService {
    //统计每小时出厂车辆
    public List<Plate> selectHourLeavePlate();
    //统计四个产品的销量
    public List<Sell_goods> selectSellGoods();
    //统计司机运货趟次
    public List<Transport> selectTransport();
    //统计钢厂的产品销售区域
    public List<Sell_address> selectSellAddress();
    //分析车牌号来自那些区域
    public List<Address> selectPlateAddress();
}
