package com.example.service;

import com.example.config.Result;
import com.example.mapper.CompanyMapper;
import com.example.pojo.Company;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface CompanyService {
    //统计公司购买重量和平均等待卸货时间
    public List<Company> selectBuyUnit();
}
