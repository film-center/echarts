package com.example.service.Impl;

import com.example.mapper.*;
import com.example.pojo.*;
import com.example.service.SelectAllDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class SelectAllDataServiceImpl implements SelectAllDataService {
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private PlateMapper plateMapper;
    @Autowired
    private Sell_addressMapper sell_addressMapper;
    @Autowired
    private Sell_goodsMapper sell_goodsMapper;
    @Autowired
    private TransportMapper transportMapper;

    @Override
    public List<Plate> selectHourLeavePlate() {
        return plateMapper.selectAll();
    }

    @Override
    public List<Sell_goods> selectSellGoods() {
        return sell_goodsMapper.selectAll();
    }

    @Override
    public List<Transport> selectTransport() {
        List<Transport> transports = transportMapper.selectAll();
        Collections.sort(transports);  //排序
        return transports;
    }

    @Override
    public List<Sell_address> selectSellAddress() {
        List<Sell_address> sellAddresses = sell_addressMapper.selectAll();
        Collections.sort(sellAddresses);
        return sellAddresses;
    }

    @Override
    public List<Address> selectPlateAddress() {
        List<Address> addressList = addressMapper.selectAll();
        Collections.sort(addressList);
        return addressList;
    }
}
