package com.example.service.Impl;

import com.example.config.Result;
import com.example.mapper.CompanyMapper;
import com.example.pojo.Company;
import com.example.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service

public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyMapper companyMapper;

    @Override
    public List<Company> selectBuyUnit() {
        List<Company> companies = companyMapper.selectCompanyNameAttr();
        Collections.sort(companies);
        return companies;
    }
}
