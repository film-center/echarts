package com.example.service.Impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.mapper.UserMapper;
import com.example.pojo.User;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int register(User user) {
        User res = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, user.getUsername()));
        if(res!=null){
            return -1;
        }
        userMapper.insert(user);
        return 0;
    }

    @Override
    public User login(User user) {
        User user1 = userMapper.selectOne(Wrappers.<User>lambdaQuery().
                        eq(User::getUsername,user.getUsername()).eq(User::getPwd,user.getPwd()));
        System.out.println("打印登录用户==>"+user1);
        return user1;
    }

    @Override
    public void update(User user) {
        userMapper.updateById(user);
    }
}
