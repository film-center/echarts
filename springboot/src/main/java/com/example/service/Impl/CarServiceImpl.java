package com.example.service.Impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mapper.CarLogisticsMapper;
import com.example.pojo.CarLogistics;
import com.example.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarLogisticsMapper carMapper;

    @Override
    public Page<CarLogistics> findPage(Integer pageNum, Integer pageSize, String search) {
        LambdaQueryWrapper<CarLogistics> wrapper = Wrappers.<CarLogistics>lambdaQuery();
        if(StrUtil.isNotBlank(search)){
            wrapper.like(CarLogistics::getUnit,search);
        }
        Page<CarLogistics> carLogisticsPage = carMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return carLogisticsPage;
    }

    @Override
    public int updateById(CarLogistics car) {
        carMapper.updateById(car);
        return 1;
    }

    @Override
    public int insert(CarLogistics car) {
        carMapper.insert(car);
        return 1;
    }

    @Override
    public int deleteById(long id) {
        carMapper.deleteById(id);
        return 1;
    }

    @Override
    public int uploadList(List<CarLogistics> carList) {
        for (CarLogistics logistics : carList) {
            logistics.setOriginId(null);
            carMapper.insert(logistics);
        }
        return 1;
    }
}
