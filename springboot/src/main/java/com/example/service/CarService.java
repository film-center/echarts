package com.example.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.pojo.CarLogistics;

import java.util.List;


public interface CarService{
    //查询所有数据
    public Page<CarLogistics> findPage(Integer pageNum, Integer pageSize, String search);
    public int updateById(CarLogistics car);
    public int insert(CarLogistics car);
    public int deleteById(long id);
    public int uploadList(List<CarLogistics> carList);
}
