package com.example.service;

import com.example.pojo.User;

public interface UserService {

    public int register(User user);
    public User login(User user);
    public void update(User user);
}
