package com.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address implements Comparable{ //销售地址
    private String cityName; //城市名称
    private int num; //相同市的车辆的和

    @Override
    public int compareTo(Object o) {//降序
        Address t = (Address) (o);
//        System.out.println(t.num - this.num);
        return t.num - this.num;
    }
}
