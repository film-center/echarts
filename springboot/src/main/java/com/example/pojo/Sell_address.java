package com.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sell_address implements Comparable {
    private String address;
    private int addressCount;

    @Override
    public int compareTo(Object o) { //降序
        Sell_address t = (Sell_address) (o);
        return  t.addressCount -this.addressCount;
    }
}
