package com.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sell_goods {//四个产品的销量
    private String goodsNameGroup; //产品名称
    private int goodsCount; //产品数量
}
