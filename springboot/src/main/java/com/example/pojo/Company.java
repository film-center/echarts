package com.example.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Company implements Comparable{
    private String CompanyName;
    private int weightCnt;
    private int minutesAvg;

    @Override
    public int compareTo(Object o) { //降序
        Company t = (Company) (o);
        return t.minutesAvg -this.minutesAvg  ;
    }
}
