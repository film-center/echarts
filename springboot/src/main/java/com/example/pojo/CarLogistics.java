package com.example.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@TableName("ods_origin")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarLogistics { //汽车物流
    @TableId(type = IdType.AUTO)
    private Long originId;
    private String orderId; //运输单编号
    private String plateNum; //车牌号
    private String unit; //收货单位
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date leaveDate; //出厂时间
    private String waitTime; //等待卸货时长
    private String goodsName; //物资名称
    private float weight; //物资重量
    private String address; //收货地址
    private String deliverUnit; //发货单位
}
