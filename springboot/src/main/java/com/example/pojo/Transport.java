package com.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transport implements Comparable{ //车辆分类
    private String  plateNum; //车牌号
    private  int plateCount;

    @Override
    public int compareTo(Object o) {//降序
        Transport t = (Transport)(o);
        return t.plateCount - this.plateCount;
    }
}
