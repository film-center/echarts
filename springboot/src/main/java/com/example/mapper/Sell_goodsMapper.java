package com.example.mapper;

import com.example.pojo.Sell_goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface Sell_goodsMapper {
    @Select("select * from dw_sell_goods")
    public List<Sell_goods> selectAll();
}
