package com.example.mapper;

import com.example.pojo.Plate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PlateMapper {
    @Select("select * from dw_hour_leave_plate")
    public List<Plate> selectAll();
}
