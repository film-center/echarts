package com.example.mapper;

import com.example.pojo.Transport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TransportMapper {
    @Select("select * from dw_transport_numbers")
    public List<Transport> selectAll();
}
