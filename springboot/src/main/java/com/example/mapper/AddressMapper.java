package com.example.mapper;

import com.example.pojo.Address;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AddressMapper {
    @Select("select * from city_str")
    public List<Address> selectAll();
}
