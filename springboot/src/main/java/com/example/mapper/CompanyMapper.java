package com.example.mapper;

import com.example.pojo.Company;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CompanyMapper {
    //统计公司购买重量和平均等待卸货时间
    public List<Company> selectCompanyNameAttr();
}
