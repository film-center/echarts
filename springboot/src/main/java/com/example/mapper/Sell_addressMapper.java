package com.example.mapper;

import com.example.pojo.Sell_address;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface Sell_addressMapper {
    @Select("select * from dw_sell_address")
    public List<Sell_address> selectAll();
}
