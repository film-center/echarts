package com.example.controller;

import com.example.config.Result;
import com.example.pojo.CarLogistics;
import com.example.service.Impl.CarServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/files")
@RestController
public class FileController {

    @Autowired
    CarServiceImpl carService;

    @PostMapping("/file")
    public Result<?>uploadData(@RequestBody List<CarLogistics> carList){
        carService.uploadList(carList);
        return Result.success();
    }
}
