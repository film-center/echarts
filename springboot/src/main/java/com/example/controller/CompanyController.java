package com.example.controller;

import com.example.config.Result;
import com.example.pojo.Company;
import com.example.service.Impl.CompanyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompanyController {

    @Autowired
    CompanyServiceImpl companyService;

    @GetMapping("/companyAll")
    public Result<?> companyAll(){
        List<Company> companies = companyService.selectBuyUnit();
        return Result.success(companies);
    }
}
