package com.example.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.config.Result;
import com.example.mapper.UserMapper;
import com.example.pojo.CarLogistics;
import com.example.pojo.User;
import com.example.service.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserServiceImpl userService;
    @Autowired
    UserMapper userMapper;

    @PostMapping("/login")
    public Result<?> login(@RequestBody User user){
        System.out.println("===>登录账号"+user);
        User res = userService.login(user);
        if(res == null){
            return Result.error("1","用户名或密码错误");
        }else {
            return  Result.success(res);
        }
    }

    @PostMapping("/register")  //新增用户和注册用户
    public Result<?> register(@RequestBody User user){
        System.out.println("注册用户===>"+user);
        int res = userService.register(user);
        if(res == -1){
            return Result.error("1","用户名重复");
        }else {
            return Result.success();
        }
    }
    @PostMapping("/hello")
    public String hell(){
        System.out.println("hello");
        return "hello";
    }
    @PutMapping("/update")
    public Result<?> update(@RequestBody User user){
        System.out.println("更新用户"+user);
        userService.update(user);
        return Result.success();
    }

    @GetMapping("/toPage")
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search) {
        System.out.println("=====>分页查询");
        LambdaQueryWrapper<User> wrapper = Wrappers.<User>lambdaQuery();
        if(StrUtil.isNotBlank(search)){
            wrapper.like(User::getNickName,search);
        }
        Page<User> userPage = userMapper.selectPage(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(userPage);
    }
    @DeleteMapping("/{originId}")
    public Result<?> delete(@PathVariable long originId){
        System.out.println("删除用户"+originId);
        userMapper.deleteById(originId);
        return Result.success();
    }
}
