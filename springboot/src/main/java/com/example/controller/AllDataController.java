package com.example.controller;

import com.example.config.Result;
import com.example.pojo.*;
import com.example.service.Impl.CompanyServiceImpl;
import com.example.service.Impl.SelectAllDataServiceImpl;
import com.example.service.SelectAllDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AllDataController {
    @Autowired
    SelectAllDataServiceImpl selectAllDataService;

    @RequestMapping("/hourPlate")
    public Result<?> HourLeavePlate(){
        List<Plate> plates = selectAllDataService.selectHourLeavePlate();
        return Result.success(plates);
    }

    @RequestMapping("/SellGood")
    public Result<?> SellGoods(){
        List<Sell_goods> sell_goods = selectAllDataService.selectSellGoods();
        return Result.success(sell_goods);
    }
    @RequestMapping("/transport")
    public Result<?> Transport(){
        List<Transport> transports = selectAllDataService.selectTransport();
        return Result.success(transports);
    }
    @RequestMapping("/SellAddress")
    public Result<?> SellAddress(){
        List<Sell_address> sellAddress = selectAllDataService.selectSellAddress();
        return Result.success(sellAddress);
    }
    @RequestMapping("/PlateAddress")
    public Result<?> PlateAddress(){
        List<Address> address = selectAllDataService.selectPlateAddress();
        address.forEach(System.out::println);
        return Result.success(address);
    }

}
