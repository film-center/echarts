package com.example.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.config.Result;
import com.example.pojo.CarLogistics;
import com.example.service.Impl.CarServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    CarServiceImpl carService;

    @GetMapping("/toPage")
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search) {
        System.out.println("=====>分页查询");
        Page<CarLogistics> page = carService.findPage(pageNum, pageSize, search);
        return Result.success(page);
    }
    @PutMapping("/update")
    public Result<?> update(@RequestBody CarLogistics car){
        System.out.println("===>修改用户:"+car);
        carService.updateById(car);
        return Result.success();
    }
    @PostMapping("/add")
    public Result<?> save(@RequestBody CarLogistics car){
        System.out.println("=====>添加用户:"+car);
        carService.insert(car);
        return Result.success();
    }
    @DeleteMapping("/{originId}")
    public Result<?> delete(@PathVariable long originId){
        System.out.println("删除用户"+originId);
        carService.deleteById(originId);
        return Result.success();
    }
}
