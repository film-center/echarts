package com.example;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mapper.CompanyMapper;
import com.example.pojo.*;
import com.example.service.Impl.CarServiceImpl;
import com.example.service.Impl.CompanyServiceImpl;
import com.example.service.Impl.SelectAllDataServiceImpl;
import com.example.service.Impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Springboot2ApplicationTests {
	@Autowired
	private CompanyServiceImpl companyMapper;

	@Autowired
	SelectAllDataServiceImpl selectAllDataService;

	@Autowired
	CarServiceImpl carService;

	@Autowired
	UserServiceImpl userService;

	@Test
	void contextLoads() {
		Page<CarLogistics> page = carService.findPage(1, 10, null);
		System.out.println(page);

	}
	@Test
	void UserTest(){
		User user = new User();
		user.setUsername("admin");
		user.setPwd("123456");
		User login = userService.login(user);
		System.out.println("打印登录用户==>"+login);
	}

	@Test
	void DataTest(){
		List<Company> companies = companyMapper.selectBuyUnit();
		for (Company company : companies) {
			System.out.println(company);
		}
	}

	@Test
	//测试查询的五个表
	void selectData(){
//		List<Plate> plates = selectAllDataService.selectHourLeavePlate();
//		for (Plate plate : plates) {
//			System.out.println(plate);
//		}
		List<Address> address = selectAllDataService.selectPlateAddress();
		for (Address address1 : address) {
			System.out.println(address1);
		}
//		List<Sell_address> sellAddress = selectAllDataService.selectSellAddress();
//		for (Sell_address address : sellAddress) {
//			System.out.println(address);
//		}
//		List<Sell_goods> sell_goods = selectAllDataService.selectSellGoods();
//		for (Sell_goods sell_good : sell_goods) {
//			System.out.println(sell_good);
//		}
//		List<Transport> transports = selectAllDataService.selectTransport();
//		for (Transport transport : transports) {
//			System.out.println(transport);
//		}
	}
}
